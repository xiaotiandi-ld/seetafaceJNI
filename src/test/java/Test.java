import com.cnsugar.ai.face.FaceHelper;
import com.cnsugar.ai.face.SeetafaceBuilder;
import com.cnsugar.ai.face.bean.Result;
import com.cnsugar.ai.face.utils.ImageUtils;
import com.seetaface2.model.FaceLandmark;
import com.seetaface2.model.SeetaRect;
import org.apache.commons.io.FileUtils;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;

public class Test {

    private void init() {
        SeetafaceBuilder.build();//系统启动时先调用初始化方法

        //等待初始化完成
        while (SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.LOADING || SeetafaceBuilder.getFaceDbStatus() == SeetafaceBuilder.FacedbStatus.READY) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @org.junit.Test
    public void testCompare() throws Exception {
        init();

        String img1 = "D:\\mayun\\seetafaceJNI\\img\\me4.jpg";
        String img2 = "D:\\mayun\\seetafaceJNI\\img\\me5.jpg";
        float res = FaceHelper.compare(new File(img1), new File(img2));
        System.out.println("result:" + res*100 + "%");
    }

    @org.junit.Test
    public void testRegister() throws IOException {
        //将目录下的jpg、png图片都注册到人脸库中，以文件名为key
        String path = "D:\\mayun\\seetafaceJNI\\img\\1";
        Collection<File> files = FileUtils.listFiles(new File(path), new String[]{"jpg", "png"}, false);
        for (File file : files) {
            String key = file.getName();
            try {
                FaceHelper.register(key, FileUtils.readFileToByteArray(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println(1);
    }

    @org.junit.Test
    public void testSearch() throws IOException {
        init();
        String img = "D:\\mayun\\seetafaceJNI\\img\\test\\me5.jpg";
        long l = System.currentTimeMillis();
        Result result = FaceHelper.search(FileUtils.readFileToByteArray(new File(img)));
        System.out.println("搜索结果：" + result + "， 耗时：" + (System.currentTimeMillis() - l));
    }

    @org.junit.Test
    public void testDetect() throws IOException {
        String img = "D:\\mayun\\seetafaceJNI\\img\\test\\cut\\yuantu.jpg";
        String imgBase64 = ImageUtils.file2Base64(new File(img));
        System.out.println("base64转换后结果：" + imgBase64);
        File imgFile = ImageUtils.base64ToFile(imgBase64);
        String imgType = "jpg";
        SeetaRect[] rects = FaceHelper.detect(FileUtils.readFileToByteArray(imgFile));
        if (rects != null) {
            for (SeetaRect rect : rects) {
                StringBuffer imgOutput = new StringBuffer();
                imgOutput.append("D:\\mayun\\seetafaceJNI\\img\\test\\cut\\");
                imgOutput.append(rect.x);
                imgOutput.append(".");
                imgOutput.append(rect.y);
                imgOutput.append(".");
                imgOutput.append(rect.width);
                imgOutput.append(".");
                imgOutput.append(rect.height);
                imgOutput.append(".");
                imgOutput.append(imgType);
                ImageUtils.cutImage(new FileInputStream(img),
                        new FileOutputStream(imgOutput.toString()),imgType, rect.x,rect.y,rect.width,rect.height);
                System.out.println("x="+rect.x+", y="+rect.y+", width="+rect.width+", height="+rect.height
                 + ", url=" + imgOutput);
            }
        }
    }

    @org.junit.Test
    public void testCorp() throws IOException {
        String img = "D:\\mayun\\seetafaceJNI\\img\\test\\yuantu1.jpg";
        BufferedImage image = FaceHelper.crop(FileUtils.readFileToByteArray(new File(img)));
        if (image != null) {
            ImageIO.write(image, "jpg", new File("D:\\mayun\\seetafaceJNI\\img\\test\\corp-face1.jpg"));
        }
    }

    @org.junit.Test
    public void testDelete() {
        FaceHelper.removeRegister("me4.jpg");
    }

    @org.junit.Test
    public void detectLandmark() throws IOException {
        String img = "D:\\mayun\\seetafaceJNI\\img\\me1.jpg";
        FaceLandmark detect = FaceHelper.detectLandmark(ImageIO.read(new File(img)));
        if (detect != null) {
            System.out.println("detect: " + detect.toString());
        }
    }

}
